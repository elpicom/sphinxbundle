<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 10/29/13
 * Time: 8:25 PM
 * To change this template use File | Settings | File Templates.
 */
namespace Elpicom\SphinxBundle\Index;

class Collection
{
    protected $indexes = array();

    public function addIndex(IndexInterface $indexService, $indexName){
        $this->indexes[$indexName] = $indexService;
    }

    public function getIndex($indexName){
        if(isset($this->indexes[$indexName])){
            return $this->indexes[$indexName];
        } else {
            return null;
        }
    }

    public function getAll(){
        return $this->indexes;
    }
}