<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 10/29/13
 * Time: 8:00 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Elpicom\SphinxBundle\Index;



abstract class SimpleIndex extends BasicIndex implements IndexInterface
{

    protected $attributes = array();


    protected $fields = array();

    protected $writer;

    protected $doctrine;

    protected $entityName = null;



    public function setDoctrine($doctrine){
        $this->doctrine = $doctrine;
    }

    protected function getDoctrine(){
        return $this->doctrine;
    }

    public function getAttributes(){
        return $this->attributes;
    }

    protected function getFields(){
        return $this->fields;
    }

    public abstract function getFileName();


    public function generate(){
        $this->openWriter();
        $this->createShema();
        $this->printRecords();
        $this->closeWriter();
        return true;
    }



    protected function getGetterName($field){
        if(strpos($field, '_') !== false){
            $parts = explode('_', $field);
            foreach($parts as $key => $val){
                $parts[$key] = ucfirst($val);
            }
            $methodName = 'get' . implode('', $parts);
        } else {
            $methodName = 'get' . ucfirst($field);
        }
        return $methodName;
    }


    protected function printRecord($entity){

        $this->writer->startElement('sphinx:document');
        $this->writer->writeAttribute('id', $entity->getId());

        foreach($this->getFields() as $name => $params){
            if(isset($params['data_method'])){
                $getterName = $params['data_method'];
            } else {
                $getterName = $this->getGetterName(substr($name, 2));
            }
            $rawValue = $entity->$getterName();

            if(empty($rawValue)){
                continue;
            }

            if(!is_array($rawValue)){
                $rawValue = array($rawValue);
            }

            $values = array();
            foreach($rawValue as $val){
                if($val instanceof \Elpicom\HrBase\StoreBundle\Entity\Bases\Base){
                    $values[] = trim($val->getTitle());
                }else {
                    $values[] = trim($val);
                }
            }

            if(isset($params['transform'])){
                $method = $params['transform'];
                $values = $this->$method($values);
            }

            $value = implode('|', $values);

            $this->writer->startElement($name);
            $this->writer->writeCdata(filter_var($value, FILTER_SANITIZE_STRING, array('flags' => FILTER_FLAG_STRIP_LOW)));
            $this->writer->endElement();
        }

        foreach ($this->getAttributes() as $name => $params){
            if(isset($params['data_method'])){
                $getterName = $params['data_method'];
            } else {
                $getterName = $this->getGetterName($name);
            }
            $rawValue = $entity->$getterName();

            if(is_null($rawValue)){
                continue;
            }


            //TODO  очень грязно, разобратся
            if(isset($params['transform']) && $params['transform'] == 'currencyToUsd'){
                $method = $params['transform'];
                $rawValue = $this->currencyToUsd($rawValue, $entity->getCurrency());
            } else {

            if(isset($params['transform'])){
                $method = $params['transform'];
                $rawValue = $this->$method($rawValue);
            }

            }

            if(isset($params['multi']) && $params['multi']){
                $values = array();
                foreach($rawValue as $item){
                    $values[] = $this->transformValueDefault($item, $params['type'], $name);
                }
                $value = implode(',', $values);
            } else {
                if(is_array($rawValue)){
                    throw new Exception ('cant transfrom array, if its mutiple value set multi=true in attribute params');
                }

                $value = $this->transformValueDefault($rawValue, $params['type'], $name);
            }

            $this->writer->writeElement($name, $value);
        }
        $this->writer->endElement(); // document*/


    }


    protected function StrVlDelimiter($val){
        return explode('|', $val);
    }


    protected function transformValueDefault($value, $type, $attribute){

        //TODO
        if($value instanceof \Elpicom\HrBase\StoreBundle\Entity\Bases\Base){
            $value = $value->getTitle();
        }

        switch ($type){
            case 'bool':
            case 'int':
                $value = (int) $value;
                break;
            case 'string':
                $value = crc32((string) $value);
                break;

            case 'datetime':
                if(is_string($value)){
                    $value = strtotime($value);
                }elseif($value instanceof \DateTime){
                    $value = $value->getTimestamp();
                } elseif(is_int($value)){
                    //$value =
                } else {
                    //TODO
                }
                break;

            // дописать если другие появятся
            default:
                throw new \Exception('transform: incorrect param type ' . $type . ' for attribute ' . $attribute);
                break;
        }

        return $value;
    }

    protected function getEntityName() {
        return $this->entityName;


    }


    protected function printRecords(){
        //TODO оптимизация
        $dql = "SELECT t FROM " .$this->getEntityName() . " t";

        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery($dql);
        // ->setFirstResult(0)
        //->setMaxResults(10);

        $candidates = $query->getResult();

        foreach($candidates as $candidate){
            $this->printRecord($candidate);
        }
    }

    ///////////////////////////

    protected function createShema(){
        //title
        $this->writer->startElement('sphinx:schema');

        //fields
        foreach($this->getFields() as $field => $params){
            $this->writer->startElement('sphinx:field');
            $this->writer->writeAttribute('name', $field);
            $this->writer->endElement();
        }

        foreach ($this->getAttributes() as $name => $params){
            $this->writer->startElement('sphinx:attr');
            $this->writer->writeAttribute('name', $name);

            //TODO
            if(isset($params['multi']) && $params['multi']) {
                $this->writer->writeAttribute('type', 'multi');

            } else {

                switch($params['type']){
                    case 'int':
                        $this->writer->writeAttribute('type', 'int');
                        if(isset($params['bits'])){
                            $this->writer->writeAttribute('bits', $params['bits']);
                        }
                        break;
                    case 'bool':
                        $this->writer->writeAttribute('type', 'int');
                        $this->writer->writeAttribute('bits', 1);
                        break;

                    case 'string':
                        $this->writer->writeAttribute('type', 'int');
                        $this->writer->writeAttribute('bits', 32);
                        break;

                    case 'datetime':
                        $this->writer->writeAttribute('type', 'timestamp');
                        break;

                    default:
                        throw new \Exception('incorrect param type ' . $params['type'] . ' for attribute ' . $name);
                        break;
                }
            }

            $this->writer->endElement();

        }

        $this->writer->endElement(); // shema
    }

    protected function openWriter(){
        $this->writer = new \XMLWriter();
        $this->writer->openURI($this->getFileName());
        $this->writer->setIndent(true);
        $this->writer->startDocument('1.0', 'UTF-8');
        $this->writer->startElement('sphinx:docset');
    }

    protected function closeWriter(){
        $this->writer->endElement(); //docset
        $this->writer->flush();
    }


    //TODO на рефакторинг
    protected $currencyConverter = null;

    public function setCurrencyConverter($currencyConverter){
        $this->currencyConverter = $currencyConverter;
    }

    protected function currencyToUsd($amount, $currency){
       return $this->currencyConverter->convert($amount, $currency, 'USD');
    }





    protected function clearPhones($values){
        $res = array();

        foreach($values as $value){
            $res[] = str_replace(array('(', ')', ' ', '-', '+'), '', $value);
        }

        return $res;
    }



}