<?php

namespace Elpicom\SphinxBundle\Command\Indexer;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


#use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Process;

/**
 * @author Antoine Hérault <antoine.herault@gmail.com>
 */
class IndexCreateCommand extends ContainerAwareCommand
{

    protected $exlude_generate_index_type = array('distributed');

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('elpicom:sphinx:indexer:create-index')
            ->setDescription('Initial indexation')
            ->addArgument(
                'indexes',
                InputArgument::IS_ARRAY,
                'Indexes')
            ->addOption(
                'all',
                null,
                InputOption::VALUE_NONE,
                'All indexes')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Indexes create mode (create, rotate, recreate)',
                'rotate'
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //TODO
        ini_set('memory_limit', "1024M");


        $inputIndexes = $input->getArgument('indexes');
        $all = $input->getOption('all');
        $mode = $input->getOption('mode');


        $config = $this->getContainer()->getParameter('sphinx');


        //TODO в конфиг
        $indexerPath = $config['indexer_path'];
        $configPath = $config['config_path'];

        $indexCollection = $this->getContainer()->get('sphinxsearch.index.collection');


        $indexes = array();
        if($all){
            $indexes = $indexCollection->getAll();
        }elseif(!empty($inputIndexes)){
            foreach($indexCollection->getAll() as $index){
                $indexName = $index->getIndexName();
                if(in_array($indexName, $inputIndexes)){
                    $indexes[$indexName] = $index;
                }
            }
        }

        // generating
        foreach($indexes as $index){
            if (!in_array($index->getType(), $this->exlude_generate_index_type)) {
                $success = $index->generate();
            }
        }

        $processLine = $indexerPath . ' --config ' . $configPath;

        foreach($indexes as $indexName => $index){
            if (!in_array($index->getType(), $this->exlude_generate_index_type)) {
                $processLine .= ' ' . $indexName;
            }
        }


        switch($mode){
            case 'rotate':
                $processLine .= ' --rotate';
                break;


            case 'recreate':
                //TODO очистить сущесвующие
            case 'create':
                if(!file_exists($config['indexes_path'])){
                    mkdir($config['indexes_path'], "0777", true);
                }
                break;
        }

        $this->runProcessWithOutput($processLine);

        if(in_array($mode, array('create', 'recreate'))){
            $this->runProcessWithOutput($config['searchd_path'] . ' --config ' . $config['config_path'] . " --stop");
            $this->runProcessWithOutput($config['searchd_path'] . ' --config ' . $config['config_path'] );
        }

        //TODO логирование и выдача (все опционально)
        //$indexer->getOutput();

        $output->writeln('indexation finised');
    }



    protected function runProcessWithOutput($command){

        echo "\n\n command " . $command . "\n\n";

        $process = new Process($command);
        $process->run(function ($type, $buffer) {
                if (Process::ERR === $type) {
                    echo 'ERR > '.$buffer;
                } else {
                    echo 'OUT > '.$buffer;
                }
            });
        return true;
    }



}
