<?php

namespace Elpicom\SphinxBundle\Command\Indexer;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


use Symfony\Component\Process\Process;

/**
 * @author Antoine Hérault <antoine.herault@gmail.com>
 */
class UpdateConfigCommand extends ContainerAwareCommand
{

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('elpicom:sphinx:config:update')
            ->setDescription('Compiling sphinx config file');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $indexCollection = $this->getContainer()->get('sphinxsearch.index.collection');

        $config = $this->getContainer()->getParameter('sphinx');

        $indexesDir = rtrim($config['indexes_path'], DIRECTORY_SEPARATOR);
        if(!file_exists($indexesDir)){


            //TODO права
            if(!mkdir($indexesDir, 0777, true)){
                throw new \Exception("can not create indexes directory " . $indexesDir);
            }
        }

        $indexesTplData = array();

        foreach($indexCollection->getAll() as $index){
            $indexPath = $indexesDir . DIRECTORY_SEPARATOR . $index->getIndexName();
            $indexesTplData[] = array(
                'name' => $index->getIndexName(),
                'source_file' => $index->getFilename(),
                'path' => $indexPath,
                'type' => $index->getType(),
                'distributed_indexes' => $index->getDistributedIndexes()
            );
        }

        $configContent = $this->getContainer()->get('twig')->render(
            'ElpicomSphinxBundle:Config:config.conf.twig',
            array('indexes' => $indexesTplData, 'config' => $config)
        );

        file_put_contents($config['config_path'], $configContent);

        $this->runProcessWithOutput($config['searchd_path'] . ' --config ' . $config['config_path'] . " --stop");
        $this->runProcessWithOutput($config['searchd_path'] . ' --config ' . $config['config_path'] );

        $output->writeln('updating config finished successfully');
    }

    protected function runProcessWithOutput($command){

        echo "\n\n command " . $command . "\n\n";

        $process = new Process($command);
        $process->run(function ($type, $buffer) {
                if (Process::ERR === $type) {
                    echo 'ERR > '.$buffer;
                } else {
                    echo 'OUT > '.$buffer;
                }
            });
        return true;
    }


}
