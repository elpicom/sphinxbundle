<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 10/31/13
 * Time: 7:09 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Elpicom\SphinxBundle\Searcher;

abstract class BasicSearcher
{

    /**
     * @var \Elpicom\SphinxBundle\API\SphinxClient
     */
    protected $sphinxClient;

    protected $index;

    public function __construct($sphinxClient){
        $this->sphinxClient = $sphinxClient;
    }

    public function setIndex($index){
        $this->index = $index;
    }

}