<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 11/3/13
 * Time: 5:53 PM
 * To change this template use File | Settings | File Templates.
 */
namespace Elpicom\SphinxBundle\Utils;

class CurrencyConverter
{

    protected $rates;
    protected $cache;


    const CURRENCY_USD = 'USD';
    const CURRENCY_BYR = 'BYR';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_EUR = 'EUR';

    protected $map = array('€' => 'EUR', '$' => 'USD', 'б.р.' => 'BYR', 'р.' => 'RUB');

    public function convert($amount, $from, $to){
        $rates = $this->getRates();

        if(!isset($rates[$from]) || !isset($rates[$to]) || (int)$amount == 0){
            return null;
        }

        $rawAmount = $amount / $rates[$to] * $rates[$from];
        $resAmount = round($rawAmount, 4);

        return $resAmount;
    }


    public function getCurrencyCode($input){

        // если передали код
        if(in_array($input, $this->map)){
            return $input;
        }

        return (isset($this->map[$input]))?$this->map[$input]:null;
    }

    public function getCurrencyName($code){

        // если передали имя
        if(isset($this->map[$code])){
            return $code;
        }

        if(($key = array_search($code, $this->map)) !== false){
           return $key;
        }

        return null;
    }


    public function setCache($cache){
        $this->cache = $cache;
    }

    protected function fetchRates(){
        $sourceUrl = 'http://nbrb.by/Services/XmlExRates.aspx?ondate=' .  date('m/d/Y');

        $xml = @simplexml_load_file($sourceUrl);
        if(!$xml){
            return false;
        }

        $rates = array();

        $xml->xpath('/DailyExRates');
        foreach($xml->{'Currency'} as $currency){
            $rates[(string)$currency->{'CharCode'}] = (float)$currency->{'Rate'};
        }

        $rates['BYR'] = 1;

        return $rates;
    }

    protected function getRates(){
        //TODO на редис? файл?  если сервер долго недоступен чтобы вообще без курсов не оказатся
        $keyUpdate = 'currency_asdfsdfwer_key_update';
        $keyCurrrency = 'currency_sdfwer';

        if(!$this->cache->get($keyUpdate)){
        //if(true){
            $rates = $this->fetchRates();
            if(!empty($rates)){
                $this->cache->set($keyCurrrency, $rates, 60*60*24*20);
                $this->cache->set($keyUpdate, true, 60*60*12);
                return $rates;
            } else{
                $this->cache->set($keyUpdate, true, 60*60);
                return $this->cache->get($keyCurrrency);
            }
        } else{
            return $this->cache->get($keyCurrrency);
        }
   }

}