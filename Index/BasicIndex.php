<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 10/31/13
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Elpicom\SphinxBundle\Index;


abstract class BasicIndex implements IndexInterface
{

    protected $indexName;
    protected $type = 'xmlpipe2';
    protected $distributed_indexes = array();


    public function getIndexName()
    {
        return $this->indexName;
    }

    public function setIndexName($name)
    {
        $this->indexName = $name;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setDistributedIndexes(array $distributed_indexes)
    {
        $this->distributed_indexes = $distributed_indexes;
    }

    public function getDistributedIndexes()
    {
        return $this->distributed_indexes;
    }

}