<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pavelpipkin
 * Date: 10/29/13
 * Time: 8:18 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Elpicom\SphinxBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class Indexes implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {

        if (!$container->hasDefinition('sphinxsearch.index.collection')) {
            return;
        }

        $definition = $container->getDefinition(
            'sphinxsearch.index.collection'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'elpicom.sphinxsearch.index'
        );

        foreach($taggedServices as $id => $tagAttributes){
            //TODO разобратся
            foreach ($tagAttributes as $attributes) {
                $container->getDefinition($id)->addMethodCall('setIndexName', array($attributes["index"]));
            }
        }


        foreach ($taggedServices as $id => $tagAttributes) {
            //TODO test
            foreach ($tagAttributes as $attributes) {
                $definition->addMethodCall(
                    'addIndex',
                    array(new Reference($id), $attributes["index"])
                );
            }
        }

    }


}