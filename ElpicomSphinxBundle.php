<?php

namespace Elpicom\SphinxBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Elpicom\SphinxBundle\DependencyInjection\Compiler\Indexes;

class ElpicomSphinxBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new Indexes());


    }


}
