<?php

namespace Elpicom\SphinxBundle\Index;



interface IndexInterface
{
    public function generate();

    public function getFileName();

    public function getIndexName();

    public function setIndexName($name);

    public function getAttributes();

}
